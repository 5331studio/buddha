/**
 * LISCENCE
 */
var buddha;
(function(buddha){
	
	
	//EXTEND JavaScript 
    (function(){if (typeof Array.prototype.reIndexOf === 'undefined') {
	    
	    //Add Regex version of indexOf type to Array
	    Array.prototype.regexOf = function (rx) {
		    var result =[]
	        for (var i in this) {
	            if (this[i].toString().match(rx)) {
		            result.push(this[i].toString())
	                //return i;
	            }
	        }
	        return result;
	    };
	    
	    
	}
	})();
	

    //Private Variables and Functions
    var EventMgmt = {
                            addEvent: function(node,event,handler){
                                if(node.addEventListener){
                                    node.addEventListener(event,handler,false);
                                }else if(node.attachEvent){
                                    node.attachEvent("on"+event,handler);
                                }else{
                                    node["on"+event] = handler;
                                }
                            },
                            
                            deleteEvent: function(node,event,handler){
                                if(node.removeEventListener){
                                    node.removeEventListener(event,handler,false);
                                }else if(node.detachEvent){
                                    node.detachEvent(event,handler);
                                }else{
                                    node["on"+event] = null;
                                }
                            }
                        }
    
    
    
    //Singleton Containing Applications Listing of Objects
    var applicationShare = function(){
       var applications =[];
        return {
                    addApplication: function(appName,appInstance){
                                                        var object =new Object();
                                                        object[appName] = appInstance;
                                                        applications.push(object)
                                                        //console.log(applications)
                                                    },
                    getApplications: function(){
                                                    //console.log(applications)
                                                    return applications;
                                                }
                }
    }();    
    
    //Application Constructor
    var bodhi = function(appName,dependencies){
           // console.log(dependencies)
            if(dependencies && (dependencies instanceof Array) && dependencies.length  > 0){
                this.dependencies={};
                for(var i=0,len = dependencies.length;i<len;i++){
                    this.dependencies[dependencies[i].valueOf()] = dependencies[i]
                }
                i=null;len=null;
            }
           
            this.appName = appName;
            this.currentScope={};
            applicationShare.addApplication(this.appName,this)
            this.eventHandler();
            return this
    };
    
    //Get List of other Applications that are on the page
    bodhi.prototype.getApplications = function(){
        this.listOfApplications = applicationShare.getApplications();
        return this;
    }
    
    //AJAX
    bodhi.prototype.request = function(config){
        if(config.method && config.url){
            var request = new XMLHttpRequest();
            
            //Success
            if(config.success){request.onload = function(){
                                    if(request.status >=200 && request.status <400){ 
                                        if(request.getResponseHeader('Content-Type') == "application/json"  ) {
                                             config.success(JSON.parse(request.responseText));
                                        }else{
                                            config.success(request.responseText);
                                        }
                                    }else{
                                        if(!config.error){
                                            console.log("Request Failed: ", request.responseText);
                                        }
                                    } 
                                }  
            }; 
            
            //Error
            if(config.error){request.onerror = config.error(request.response)}
            
            //Open Connection
            request.open(config.method,config.url,true);
            if(config.overrideType){
                request.overrideMimeType(config.dataType)   
            }
            
            if(typeof config.header == 'object'){
                for(var key in config.header){
                   request.setRequestHeader(key,config.header[key]); 
                }                
            }
            
            //Configure Content-Type and Send Data
            if(typeof config.method == 'string' && config.method.toLowerCase() == ("post"||"put") ){
                if(config.contentType){
                    request.setRequestHeader('Content-Type', config.contentType);  
                    request.send(JSON.stringify(config.data));
                    
                }else{
                    request.setRequestHeader('Content-Type', "application/json");  
                    request.send(JSON.stringify(config.data));
                }
                      
            }else{
                //Using Get or Delete
                request.send(config.data);
            }
            
            
        }else{
            console.log("Missing Method or URL for Request")
        }
    
    }
    
    //Router
    bodhi.prototype.dharma = function(config){
        this.config = config;
        if(this.config && typeof this.config == "object" && Object.keys(this.config).length >0 ){
            if(this.config.verdas 
                && typeof this.config.verdas == "object" 
                    && Object.keys(this.config.verdas).length >0 
                        && this.dependencies && this.dependencies.router){
                        
                        if(this.routeMgmt){ this.routeMgmt.clearHandlers(["load","hashchange"]); }
                        this.routeMgmt = new this.dependencies.router(this).bootstrap(["load","hashchange"]);                
            }      
            
        
        }else{
            throw "Missing configuration settings!";
        }
        //console.log("dharma : Place Routes here for Frameworks ", this.appName);
        return this;
    }
    
    //Promise
    bodhi.prototype.promise = function(fn) {
                                          var callback = null;
                                          this.then = function(cb) {
                                            callback = cb;
                                          };

                                          function resolve(value) {
                                            // force callback to be called in the next
                                            // iteration of the event loop, giving
                                            // callback a chance to be set by then()
                                            setTimeout(function() {
                                              callback(value);
                                            }, 1);
                                          }

                                          fn(resolve);
                                      };
    
    
    bodhi.prototype.eventHandler = function(){
	    var that = this;
	    function directEvent(e){
		    					var el = e.target,
		    						egoDatasetArray = Object.keys(el.dataset).regexOf(/(ego)(.)+/),
		    						targetAttribute = "ego" + e.type.charAt(0).toUpperCase() + e.type.substring(1, e.type.length);
		    					//LAST AREA
								if(e.type == "submit" ){
									e.preventDefault();
								}	
		    					if(Object.keys(el.dataset).length > 0 
		    							&& egoDatasetArray.length != 0
											&& egoDatasetArray.indexOf(targetAttribute.toString()) != -1){
												
			    						//console.log("HELLO",egoDatasetArray.indexOf(targetAttribute.toString()),targetAttribute);
			    						//console.log(el,el.dataset,e,Object.keys(el.dataset).regexOf(/(ego)(.)+/),that.currentScope)
			    						
			    						switch(e.type){
				    						case "submit":
				    							that.currentScope[el.dataset[targetAttribute]](e);
				    						break;
				    						default:
				    							that.currentScope[el.dataset[targetAttribute]](e);
				    						break;
			    						}
			    										    					
				    					
				    					
			    										
								
									//DETERMINE EVENT TYPE AND EXECUTE
			    					
		    					}
								
								
								
								
	    }
	    
	    var eventTypes = ["click","submit","keyup","keydown","keypress","mouseover","mouseout","focus","blur"];
	    eventTypes.forEach(function(item){
		    document.addEventListener(item, directEvent,false);	
	    });
	        
    }
    
    
    buddha.bodhi = function(appName,dependencies){ return new bodhi(appName,dependencies)};
    
    

})(buddha||(buddha={}))