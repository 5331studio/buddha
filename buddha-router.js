    function Routing(app){
        this.app = app;
        this.routes = this.app.config.verdas;
        return this;
    }

    Routing.valueOf = function (){return "router";};

    Routing.prototype.eventMgmt = {
                            addEvent: function(node,event,handler){
                                if(node.addEventListener){
                                    node.addEventListener(event,handler,false);
                                }else if(node.attachEvent){
                                    node.attachEvent("on"+event,handler);
                                }else{
                                    node["on"+event] = handler;
                                }
                            },
                            
                            deleteEvent: function(node,event,handler){
                                if(node.removeEventListener){
                                    node.removeEventListener(event,handler,false);
                                }else if(node.detachEvent){
                                    node.detachEvent(event,handler);
                                }else{
                                    node["on"+event] = null;
                                }
                            }
                        }
    
    Routing.prototype.routingHashHandler = function(event){
        
        if(location.hash in this.routes){
           if(this.routes[location.hash].phenomenon){
             var view =   ("dukkha" in this.routes[location.hash].phenomenon ? "dukkha" : null) || ("anitya" in this.routes[location.hash].phenomenon ? "anitya" : null) || ("anatman" in this.routes[location.hash].phenomenon ? "anatman" : null);
               
               switch(view){
                    case "anatman":
                           alert("anatman");
                            this.routes[location.hash].tenet();
                    break;
                    case "dukkha":
                          
                            var that=this;
                            
                            //Create Render function
                            var renderPage = function(){
                                var extension;
                                        if(that.app.config.dukkhaSufferType){ extension = that.app.config.dukkhaSufferType}else{ extension = ".html";}
                                        app.currentScope = new that.routes[location.hash].tenet();
                                        app.currentScope.doc = that.app.dependencies.renderer.templateCompiler(
                                                                that.routes[location.hash].phenomenon[view], 
                                                                app.currentScope, 
                                                                extension );
                                         
                            }
                            
                            //Recursively check to see if data is available
                            var loopIt = (function f(){
                                if(typeof promisesFulfilled !='undefined' && that.routes[location.hash].incarnation.length == promisesFulfilled.length){
                                    renderPage();                                    
                                }else{
                                    setTimeout(f,0);
                                }
                                
                            })
                            
                            
                            //If Incarnation (Resolve Promises) Found, then Loop Through Resolvers and add to promisesFulfilled Array.
                            if(this.routes[location.hash].incarnation){
                                var promisesFulfilled = [];
                                this.routes[location.hash].incarnation.forEach(function(item,index,array){
                                    item().then(function(value){
                                        promisesFulfilled.push(value);
                                    })
                                });

                                //Execute Recursive Loop
                                loopIt();
                                
                            // Else just render whats in the controller    
                            }else{
                                 renderPage();  
                            }

                           
                    break;
                    case "anitya":
                           alert("anitya");
                            this.routes[location.hash].tenet();
                    break;
                   default:
                        alert("Nada");
                   break;
               }
           
           }
            
        }else if(!location.hash){
            location.hash="#/";
        }else{
            if(this.routes.default){
                this.routes.default.tenet();
            }else{
                alert("404");
            }
            
        }
        
        //return this;
    
    }
    
    Routing.prototype.bootstrap = function(eventTypes,request){
        this.request = request;
        var that = this;
            for(var i=0, len =eventTypes.length; i<len;i++ ){
                this.eventMgmt.addEvent(window,eventTypes[i],function(event){that.routingHashHandler(event,that.request);} )
            }            
            return this;
    
    }
    
    Routing.prototype.clearHandlers = function(eventTypes,handler){
        for(var i=0, len =eventTypes.length; i<len;i++ ){
               window["on"+eventTypes[i]] = null;
            }            
            return this;
    }