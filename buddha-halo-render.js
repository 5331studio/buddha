var html = '<button>((action))</button>\n'+
				'<label>((nameLabel))</label>\n'+
				'<input value="((nameValue))">\n'+
				'<ul>((beginloop item in code))\n'+
				'<li>((item))</li>\n'+
				'((endloop))\n'+
				'</ul>';
var placeholders = html.match(/[(]{2}([0-9a-zA-Z\s])+[))]{2}/gim);
var variables = {"action":"King","nameLabel":"Anthony","nameValue":"Crawford","code":["JavaScript","CSS","HTML"]};

/*placeholders.forEach(function(item,index,array){
    console.log(    variables[item.toString().replace(/[()]+/gi,"")]      )
    console.log(item.toString())
	if(/beginloop/.test(item)){
		var itemArray = item.replace(/[()]+/gi,"").split(/\s+/gi);
		var arrItems = variables[itemArray[itemArray.length-1]]
		console.log(arrItems)
	}
    html = html.replace(item, variables[item.toString().replace(/[()]+/gi,"")])

})*/

var renderTemplate =(function f(placeholders,variables){
	placeholders.forEach(function(item,index,array){
		//console.log(    variables[item.toString().replace(/[()]+/gi,"")]      )
		//console.log(item.toString())
		if(/beginloop/.test(item)){
			var itemArray = item.replace(/[()]+/gi,"").split(/\s+/gi);
			var arrItems = variables[itemArray[itemArray.length-1]]
			//console.log(arrItems)
			f(arrItems,variables)
		}
		html = html.replace(item, variables[item.toString().replace(/[()]+/gi,"")])
	
	})
	
})

renderTemplate(placeholders,variables)


var blueprint = [
					{
						"nodeType":"input",
						"attributes":[
										{"type":"text"},
										{"data-karma-keypress":"doSomething()"}
									 ]
									
					},
					{
						"nodeType":"div",
						"attributes":[],
						"childNodes":[
										{
											"nodeType":"span",
											"innerHTML":"Hello World"
										}
									  ]
					}
				]

var nodeParser = function(obj){
	switch(obj.nodeType){
		case "text":
		break;
		case "attribute":
		break;
		case "element":
		break;		
	}
};

var renderUI = (function f(arr){
	
	var dom = arr.map(function(item,index,array){
		var node = document.createElement(item.nodeType);
		
		if(item.attributes){
			item.attributes.forEach(function(item){
				node.setAttribute(Object.keys(item)[0],item[Object.keys(item)[0]]);
			})
		}
		node.value = (item.value) ? item.value : null;
		if(item.childNodes){
			var childNodes = f(item.childNodes)
			childNodes.forEach(function(item){
				node.appendChild(item)
			})
			console.log("Children",node.childNodes)
		}
		return node;
	})
	
	return dom;
	
})

var dom = renderUI(blueprint)

console.log(dom)